import { SDBaseService } from 'app/n-services/SDBaseService';
//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-buyerservice
import { buyerservice } from '../sd-services/buyerservice';
//CORE_REFERENCE_IMPORT-sellerservice
import { sellerservice } from '../sd-services/sellerservice';
import { uiservices } from 'app/sd-services/uiservices';

export const sdProviders = [
  SDBaseService,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-buyerservice
  buyerservice,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-sellerservice
  sellerservice,
  uiservices
];
