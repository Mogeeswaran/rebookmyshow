/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Inject } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { uiservices } from '../../sd-services/uiservices';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { buyerservice } from '../../sd-services/buyerservice';
import { Router } from '@angular/router';


/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-buyerticketdetails',
    templateUrl: './buyerticketdetails.template.html'
})

export class buyerticketdetailsComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    userdata: any;
    mobile: any;
    buydata: any;
    selldata: any;
    buyerdata: any;
    sellerdata: any;
    
    constructor(private bdms: NDataModelService, private uiservice: uiservices, @Inject(MAT_DIALOG_DATA) public data,private buyerService: buyerservice, public dialogRef: MatDialogRef<buyerticketdetailsComponent>, private snackbar: NSnackbarService, private router: Router ) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        console.log(this.data);
    }   

    async buy(){
        console.log(this.data.amount);        
        this.buydata = {
            debits : this.data.amount,
            buyermobilenumber: localStorage.getItem('mobile')                               
        }
        this.selldata = {
            credits : this.data.amount,
            sellermobilenumber: this.data.mobile
        }
        console.log("selldata",this.selldata)
        this.buyerdata = (await this.buyerService.buydetails(this.buydata)).local.result;
        console.log(this.buyerdata);
        this.sellerdata = (await this.buyerService.selldetails(this.selldata)).local.result;
        console.log("sellerdata",this.sellerdata);

        if(this.buyerdata.responsecode==200&& this.sellerdata.responsecode==200){
            this.dialogRef.close();
            this.snackbar.openSnackBar('Your Ticket Successfully Booked and Your Points Debited');
            this.router.navigate(['profile']);
        }
    }  

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete (dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }
}
