/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService, NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
//import { FormGroup,FormControl } from '@angular/forms';
import { sellerservice } from '../../sd-services/sellerservice';
import {user}  from '../../models/user.model'
import { Router } from '@angular/router';



/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-seller',
    templateUrl: './seller.template.html'
})

export class sellerComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    user : user;
    movie: any;
    theatre: any;
    date: any;
    tickets: any;
    amount: any;
    mobile: any;  
    ticketDetails: any;
    time: string[] = [
        '10:30', '13:30', '16:20', '19:40', '22:20'
    ];

    constructor(private bdms: NDataModelService, private sellerService: sellerservice,  private router: Router, private snackbar: NSnackbarService) {
        super();
        this.mm = new ModelMethods(bdms);
        this.user = new user();
    }

    ngOnInit() {
        this.mobile = localStorage.getItem('mobile');
        console.log(this.mobile);
    }
    
    async onSell() {
        var ticketInfo = {
            movie : this.user.movie,
            theatre: this.user.theatre,
            date: this.user.date,
            time: this.user.time,
            tickets: this.user.tickets,
            amount: this.user.amount,
            mobile: this.mobile
        }
        console.log(ticketInfo);
        this.ticketDetails = (await this.sellerService.sendinfo(ticketInfo)).local.result; 
        console.log(this.ticketDetails);        
        this.snackbar.openSnackBar('Sold ticket Successfully you will get the credits after user bought it');
        this.router.navigate(['profile']);        
    }
    signout() {
        localStorage.removeItem('data');
        localStorage.removeItem('mobile');
        localStorage.removeItem('jwt');
        this.router.navigate(['home']);
    }
}
