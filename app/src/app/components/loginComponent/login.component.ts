/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService,NSnackbarService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { uiservices } from '../../sd-services/uiservices';
import { Router } from '@angular/router';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-login',
    templateUrl: './login.template.html'
})

export class loginComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    loginform: FormGroup;
    error:boolean;
    userdata: any;
    errormsg:any;

    constructor(private formbuilder: FormBuilder, private bdms: NDataModelService, private uiservice: uiservices, private router: Router,private snackbar: NSnackbarService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.loginform = this.formbuilder.group({
            email: ['',[Validators.required,Validators.email]],
            password: ['',[Validators.required]]
        });
    }

    async onsubmit() {
        console.log(this.loginform.value);
        if(this.loginform.valid) {  
            this.userdata = ( await this.uiservice.login(this.loginform.value)).local.result;
        }        
        console.log(this.userdata);
        if (this.userdata.status == 'success') {
            console.log(this.userdata);
            localStorage.setItem('data',this.userdata.data);
            localStorage.setItem('jwt',this.userdata.token);
            localStorage.setItem('mobile',this.userdata.mobile);
            this.router.navigate(['profile']);
        }else if(this.userdata.status == 'Error'){
            this.error = true;
             this.snackbar.openSnackBar('User not found,please Register');
        }
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }    
    get email(){
        return this.loginform.get('email');
    }
    get password(){
        return this.loginform.get('password');
    }

}
