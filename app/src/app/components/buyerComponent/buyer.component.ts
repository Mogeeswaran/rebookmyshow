/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { sellerservice } from '../../sd-services/sellerservice';
import { Router } from '@angular/router';
import { buyerticketdetailsComponent } from '../buyerticketdetailsComponent/buyerticketdetails.component';
import { MatDialog } from '@angular/material';


/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-buyer',
    templateUrl: './buyer.template.html'
})

export class buyerComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    ticketInformation: any;

    constructor(private router: Router, private bdms: NDataModelService, private sellerService: sellerservice, private modal: MatDialog) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.getInfo();

    }

    buyticket(data) {
        console.log(data);        
        const dialogRef = this.modal.open(buyerticketdetailsComponent, {
            data: data,
            width: '500px',
            disableClose: true
        });

    }

    async getInfo() {
        var status = (await this.sellerService.getinfo()).local.result;
        console.log(status);

        this.ticketInformation = this.objConvert(status);
    }
    objConvert(obj) {
        return Array.from(Object.keys(obj), k => obj[k]);

    }

    signout() {
        localStorage.removeItem('data');
        localStorage.removeItem('mobile');
        localStorage.removeItem('jwt');
        this.router.navigate(['home']);
    }

}
