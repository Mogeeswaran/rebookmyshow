/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { uiservices } from '../../sd-services/uiservices';
import { Router, CanActivate } from '@angular/router';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-profile',
    templateUrl: './profile.template.html'
})

export class profileComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    email: any;
    userdata: any;
    users: any;
    points: any;
    mobile: any;

    constructor(private bdms: NDataModelService, private uiservice: uiservices, private router: Router) {
        super();        
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.email = localStorage.getItem('data');
        this.mobile = localStorage.getItem('mobile');
        console.log(this.mobile);
        console.log(this.email);
        this.getdata();
    }

    async getdata(){    
        this.userdata = ( await this.uiservice.getuserbyemail(this.email)).local.result;
        console.log(this.userdata);
        this.points = (await this.uiservice.getpoints(this.mobile)).local.result;
        console.log(this.points);
    }

    sell(){
        this.router.navigate(['ticketseller']);
    }

    buy(){
        this.router.navigate(['ticketbuyer']);
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete (dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }
    signout() {
        localStorage.removeItem('data');
        localStorage.removeItem('mobile');
        localStorage.removeItem('jwt');
        this.router.navigate(['home']);
    }
}
