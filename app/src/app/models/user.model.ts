import {JsonProperty, JsonObject} from '../lib/tj.deserializer'

@JsonObject
export class user {
  @JsonProperty('movie', String, true)
  public movie: string = undefined;

  @JsonProperty('theatre', String, true)
  public theatre: string = undefined;

  @JsonProperty('date', Date, true)
  public date: Date = undefined;

  @JsonProperty('time', String, true)
  public time: string = undefined;

  @JsonProperty('tickets', Number, true)
  public tickets: number = undefined;

  @JsonProperty('amount', Number, true)
  public amount: number = undefined;

}