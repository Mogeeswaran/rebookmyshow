/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class sellerservice {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_sellerservice

  public async sendinfo(ticketinfo = undefined, ...others) {
    try {
      let bh = {
        input: { ticketinfo: ticketinfo },
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_W3yef5Lh0jrF0H2Y(bh);
      //appendnew_next_sendinfo
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async getinfo(...others) {
    try {
      let bh = {
        input: {},
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_8ubtdcd5Ih9CjRRO(bh);
      //appendnew_next_getinfo
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_sellerservice_Start

  async sd_W3yef5Lh0jrF0H2Y(bh) {
    try {
      bh.local.modelrApiUrl = 'http://localhost:24483/api/sendinfo';

      bh = await this.sd_Xc2JGX27vscEKNcl(bh);
      //appendnew_next_sd_W3yef5Lh0jrF0H2Y
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_Xc2JGX27vscEKNcl(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.ticketinfo
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_Xc2JGX27vscEKNcl
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_8ubtdcd5Ih9CjRRO(bh) {
    try {
      bh.local.modelrApiUrl = 'http://localhost:24483/api/getinfo';

      bh = await this.sd_3d2EpCmZAarjsBAf(bh);
      //appendnew_next_sd_8ubtdcd5Ih9CjRRO
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_3d2EpCmZAarjsBAf(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_3d2EpCmZAarjsBAf
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}
