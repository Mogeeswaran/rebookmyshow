/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class uiservices {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_uiservices

  public async login(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrapiur: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_MRcvqbam22LR5xCV(bh);
      //appendnew_next_login
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async getuserbyemail(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrurl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_2oH3zOrrc5P0t2U3(bh);
      //appendnew_next_getuserbyemail
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async register(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrurl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_g8OjqR3fbs5ZdsAb(bh);
      //appendnew_next_register
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async getticketbymobile(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { result: undefined, modelrApiUrl: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_8MiEjqW5zbYo1Ki4(bh);
      //appendnew_next_getticketbymobile
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async getpoints(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { modelrapiurl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_p3SQ3q9icCyc1FH3(bh);
      //appendnew_next_getpoints
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_uiservices_Start

  async sd_MRcvqbam22LR5xCV(bh) {
    try {
      console.log('test');
      bh.local.modelrapiur = `http://localhost:24483/api/login`;

      bh = await this.sd_tKPnPw6bjrBAWfNf(bh);
      //appendnew_next_sd_MRcvqbam22LR5xCV
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_tKPnPw6bjrBAWfNf(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrapiur,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_tKPnPw6bjrBAWfNf
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_2oH3zOrrc5P0t2U3(bh) {
    try {
      bh.local.modelrurl = `http://localhost:24483/api/profile?data=${bh.input.data}`;

      bh = await this.sd_52Slw5qw77AFPTMz(bh);
      //appendnew_next_sd_2oH3zOrrc5P0t2U3
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_52Slw5qw77AFPTMz(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrurl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_52Slw5qw77AFPTMz
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_g8OjqR3fbs5ZdsAb(bh) {
    try {
      bh.local.modelrurl = `http://localhost:24483/api/register`;

      bh = await this.sd_1TXM1E2ZJdfVOYMJ(bh);
      //appendnew_next_sd_g8OjqR3fbs5ZdsAb
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_1TXM1E2ZJdfVOYMJ(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrurl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_1TXM1E2ZJdfVOYMJ
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_8MiEjqW5zbYo1Ki4(bh) {
    try {
      bh.local.modelrApiUrl = `http://localhost:24483/api/seller?mobile=${bh.input.data}`;

      bh = await this.sd_B8FQHUEPquU0uufC(bh);
      //appendnew_next_sd_8MiEjqW5zbYo1Ki4
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_B8FQHUEPquU0uufC(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrApiUrl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_B8FQHUEPquU0uufC
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_p3SQ3q9icCyc1FH3(bh) {
    try {
      console.log(bh.input.data);
      bh.local.modelrapiurl = `http://localhost:24483/api/transactiondetails?data=${bh.input.data}`;

      bh = await this.sd_CDEOFjdzEibsfzqr(bh);
      //appendnew_next_sd_p3SQ3q9icCyc1FH3
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_CDEOFjdzEibsfzqr(bh) {
    try {
      let requestOptions = {
        url: bh.local.modelrapiurl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_CDEOFjdzEibsfzqr
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}
