/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SDBaseService } from '../n-services/SDBaseService';
import { environment } from '../../environments/environment';
import {
  NAlertComponent,
  NAlertService,
  NFileIOService,
  NFileUploadComponent
} from 'neutrinos-module';
import {
  NDataModelService,
  NAuthGuardService,
  NHTTPLoaderService,
  NLocalStorageService,
  NLoginService,
  NLogoutService,
  NNotificationService,
  NPubSubService,
  NSessionStorageService,
  NSnackbarService,
  NSystemService,
  NTokenService
} from 'neutrinos-seed-services';
//CORE_REFERENCE_IMPORTS

declare const window: any;
declare const cordova: any;

@Injectable()
export class buyerservice {
  systemService = NSystemService.getInstance();
  appProperties;

  constructor(
    private http: HttpClient,
    private matSnackBar: MatSnackBar,
    private sdService: SDBaseService,
    private sessionStorage: NSessionStorageService,
    private tokenService: NTokenService,
    private router: Router,
    private httpLoaderService: NHTTPLoaderService,
    private dataModelService: NDataModelService,
    private loginService: NLoginService,
    private authGuardService: NAuthGuardService,
    private localStorageService: NLocalStorageService,
    private logoutService: NLogoutService,
    private notificationService: NNotificationService,
    private pubsubService: NPubSubService,
    private snackbarService: NSnackbarService,
    private alertService: NAlertService,
    private fileIOService: NFileIOService
  ) {}

  //   service flows_buyerservice

  public async buydetails(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { apiurl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_TvWJXg9g2SCkSglC(bh);
      //appendnew_next_buydetails
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  public async selldetails(data = undefined, ...others) {
    try {
      let bh = {
        input: { data: data },
        local: { apiurl: undefined, result: undefined }
      };
      bh = this.__constructDefault(bh);

      bh = await this.sd_EJOClSsj9yMcYtUi(bh);
      //appendnew_next_selldetails
      //Start formatting output variables
      let outputVariables = { input: {}, local: { result: bh.local.result } };
      //End formatting output variables
      return outputVariables;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_flow_buyerservice_Start

  async sd_TvWJXg9g2SCkSglC(bh) {
    try {
      console.log(bh.input.data);
      bh.local.apiurl = `http://localhost:3000/users/buyerupdate`;

      bh = await this.sd_Ha18RTfEo9RFDbU3(bh);
      //appendnew_next_sd_TvWJXg9g2SCkSglC
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_Ha18RTfEo9RFDbU3(bh) {
    try {
      let requestOptions = {
        url: bh.local.apiurl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_Ha18RTfEo9RFDbU3
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_EJOClSsj9yMcYtUi(bh) {
    try {
      console.log('input', bh.input.data);
      bh.local.apiurl = `http://localhost:3000/users/sellerupdate`;

      bh = await this.sd_N7shUh0CQYXqp1Vd(bh);
      //appendnew_next_sd_EJOClSsj9yMcYtUi
      return bh;
    } catch (e) {
      throw e;
    }
  }
  async sd_N7shUh0CQYXqp1Vd(bh) {
    try {
      let requestOptions = {
        url: bh.local.apiurl,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.data
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_N7shUh0CQYXqp1Vd
      return bh;
    } catch (e) {
      throw e;
    }
  }
  //appendnew_node

  __constructDefault(bh) {
    const system: any = {};

    try {
      system.currentUser = this.sessionStorage.getValue('userObj');
      system.environment = environment;
      system.tokenService = this.tokenService;
      system.deviceService = this.systemService;
      system.router = this.router;
      system.httpLoaderService = this.httpLoaderService;
      system.dataModelService = this.dataModelService;
      system.loginService = this.loginService;
      system.authGuardService = this.authGuardService;
      system.localStorageService = this.localStorageService;
      system.logoutService = this.logoutService;
      system.notificationService = this.notificationService;
      system.pubsubService = this.pubsubService;
      system.snackbarService = this.snackbarService;
      system.alertService = this.alertService;
      system.fileIOService = this.fileIOService;

      Object.defineProperty(bh, 'system', {
        value: system,
        writable: false
      });

      return bh;
    } catch (e) {
      throw e;
    }
  }
}
