/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class authgaurdService implements CanActivate{
    constructor(private router: Router) {

    }
    canActivate(){
    if(localStorage.getItem('jwt') === null) {
        this.router.navigate(['']);
    }
    else{
        return true;
    }
}
}
